/* 
  This file was generated automatically with ./scripts/maple2c_new.pl.
  Do not edit this file directly as it can be overwritten!!

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

  Maple version     : Maple 2016 (X86 64 LINUX)
  Maple source      : ./maple/lda_c_pz.mpl
  Type of functional: lda_exc
*/

#define maple2c_order 3

static inline void
func_unpol(const xc_func_type *p, int order, const double *rho, double *zk, double *vrho, double *v2rho2, double *v3rho3)
{
  double t1, t2, t3, t4, t5, t6, t7, t8;
  double t9, t10, t12, t13, t17, t18, t19, t22;
  double t24, t25, t26, t27, t28, t29, t30, t31;
  double t32, t33, t34, t35, t37, t41, t42, t44;
  double t46, t47, t49, t52, t54, t55, t59, t60;
  double t61, t62, t63, t64, t65, t67, t69, t70;
  double t71, t73, t76, t79, t81, t83, t84, t85;
  double t88, t89, t91, t95, t97, t100, t102, t104;
  double t117, t118, t119, t120, t121, t123, t127, t129;
  double t132, t135, t136, t138, t141, t144, t145, t147;
  double t151, t155, t156, t158, t160, t165, t166, t170;
  double t175, t177, t180, t182, t184, t186, t187, t189;
  double t192, t194, t196, t197, t199, t202, t205, t208;
  double t210, t212, t225, t236, t237, t238, t239, t250;
  double t254, t256, t257, t273, t278, t279, t284, t302;
  double t306, t312, t327, t330, t378, t381;

  lda_c_pz_params *params;

  assert(p->params != NULL);
  params = (lda_c_pz_params * )(p->params);

  t1 = params->gamma[0];
  t2 = params->beta1[0];
  t3 = M_CBRT3;
  t4 = 0.1e1 / M_PI;
  t5 = POW_1_3(t4);
  t6 = t3 * t5;
  t7 = M_CBRT4;
  t8 = t7 * t7;
  t9 = POW_1_3(rho[0]);
  t10 = 0.1e1 / t9;
  t12 = t6 * t8 * t10;
  t13 = sqrt(t12);
  t17 = params->beta2[0] * t3;
  t18 = t5 * t8;
  t19 = t18 * t10;
  t22 = 0.1e1 + t2 * t13 / 0.2e1 + t17 * t19 / 0.4e1;
  t24 = t1 / t22;
  t25 = t12 / 0.4e1;
  t26 = 0.1e1 - t25;
  t27 = Heaviside(t26);
  t28 = t24 * t27;
  t29 = params->a[0];
  t30 = t27 * t29;
  t31 = log(t25);
  t32 = t30 * t31;
  t33 = params->b[0];
  t34 = t27 * t33;
  t35 = params->c[0];
  t37 = t27 * t35 * t3;
  t41 = t37 * t18 * t10 * t31 / 0.4e1;
  t42 = params->d[0];
  t44 = t27 * t42 * t3;
  t46 = t44 * t19 / 0.4e1;
  if(zk != NULL && (p->info->flags & XC_FLAGS_HAVE_EXC))
    *zk = t24 - t28 + t32 + t34 + t41 + t46;

  if(order < 1) return;


  t47 = t22 * t22;
  t49 = t1 / t47;
  t52 = t2 / t13 * t3;
  t54 = 0.1e1 / t9 / rho[0];
  t55 = t18 * t54;
  t59 = -t17 * t55 / 0.12e2 - t52 * t55 / 0.12e2;
  t60 = t49 * t59;
  t61 = t27 * t59;
  t62 = t49 * t61;
  t63 = 0.0;
  t64 = t24 * t63;
  t65 = t8 * t54;
  t67 = t64 * t6 * t65;
  t69 = t63 * t3;
  t70 = t69 * t5;
  t71 = t29 * t31;
  t73 = t70 * t65 * t71;
  t76 = t30 / rho[0];
  t79 = t70 * t65 * t33;
  t81 = t3 * t3;
  t83 = t5 * t5;
  t84 = t63 * t81 * t83;
  t85 = t9 * t9;
  t88 = t7 / t85 / rho[0];
  t89 = t35 * t31;
  t91 = t84 * t88 * t89;
  t95 = t37 * t18 * t54 * t31;
  t97 = t37 * t55;
  t100 = t84 * t88 * t42;
  t102 = t44 * t55;
  t104 = -t60 + t62 - t67 / 0.12e2 + t73 / 0.12e2 - t76 / 0.3e1 + t79 / 0.12e2 + t91 / 0.12e2 - t95 / 0.12e2 - t97 / 0.12e2 + t100 / 0.12e2 - t102 / 0.12e2;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vrho[0] = rho[0] * t104 + t24 - t28 + t32 + t34 + t41 + t46;

  if(order < 2) return;


  t117 = 0.0;
  t118 = t117 * t4;
  t119 = rho[0] * rho[0];
  t120 = t119 * rho[0];
  t121 = 0.1e1 / t120;
  t123 = t118 * t121 * t42;
  t127 = t118 * t121 * t35 * t31;
  t129 = t49 * t69;
  t132 = t129 * t18 * t54 * t59;
  t135 = 0.1e1 / t9 / t119;
  t136 = t8 * t135;
  t138 = t64 * t6 * t136;
  t141 = t70 * t136 * t71;
  t144 = 0.1e1 / t85 / t119;
  t145 = t7 * t144;
  t147 = t84 * t145 * t89;
  t151 = t37 * t18 * t135 * t31;
  t155 = t1 / t47 / t22;
  t156 = t59 * t59;
  t158 = t155 * t27 * t156;
  t160 = t155 * t156;
  t165 = t2 / t13 / t12 * t81;
  t166 = t83 * t7;
  t170 = t18 * t135;
  t175 = -t165 * t166 * t144 / 0.18e2 + t52 * t170 / 0.9e1 + t17 * t170 / 0.9e1;
  t177 = t49 * t27 * t175;
  t180 = t30 / t119;
  t182 = t49 * t175;
  t184 = t70 * t136 * t29;
  t186 = t117 * t81;
  t187 = t186 * t83;
  t189 = t187 * t145 * t33;
  t192 = t84 * t145 * t35;
  t194 = t37 * t170;
  t196 = t24 * t117;
  t197 = t81 * t83;
  t199 = t196 * t197 * t145;
  t202 = t187 * t145 * t71;
  t205 = t70 * t136 * t33;
  t208 = t84 * t145 * t42;
  t210 = t44 * t170;
  t212 = t180 / 0.3e1 - t182 - t184 / 0.18e2 + t189 / 0.36e2 - t192 / 0.18e2 + 0.5e1 / 0.36e2 * t194 - t199 / 0.36e2 + t202 / 0.36e2 - t205 / 0.9e1 - t208 / 0.6e1 + t210 / 0.9e1;
  if(v2rho2 != NULL && (p->info->flags & XC_FLAGS_HAVE_FXC))
    v2rho2[0] = -0.2e1 * t60 + 0.2e1 * t62 - t67 / 0.6e1 + t73 / 0.6e1 - 0.2e1 / 0.3e1 * t76 + t79 / 0.6e1 + t91 / 0.6e1 - t95 / 0.6e1 - t97 / 0.6e1 + t100 / 0.6e1 - t102 / 0.6e1 + rho[0] * (t212 + t123 / 0.12e2 + t127 / 0.12e2 + t132 / 0.6e1 + t138 / 0.9e1 - t141 / 0.9e1 - t147 / 0.6e1 + t151 / 0.9e1 - 0.2e1 * t158 + 0.2e1 * t160 + t177);

  if(order < 3) return;


  t225 = t123 / 0.4e1 + t127 / 0.4e1 + t132 / 0.2e1 + t138 / 0.3e1 - t141 / 0.3e1 - t147 / 0.2e1 + t151 / 0.3e1 - 0.6e1 * t158 + 0.6e1 * t160 + 0.3e1 * t177 + t180;
  t236 = 0.0;
  t237 = t236 * t4;
  t238 = t119 * t119;
  t239 = 0.1e1 / t238;
  t250 = t239 * t35;
  t254 = t47 * t47;
  t256 = t1 / t254;
  t257 = t156 * t59;
  t273 = 0.1e1 / t85 / t120;
  t278 = 0.1e1 / t9 / t120;
  t279 = t18 * t278;
  t284 = -t2 / t13 / t197 / t7 * t85 * t4 * t239 / 0.12e2 + 0.2e1 / 0.9e1 * t165 * t166 * t273 - 0.7e1 / 0.27e2 * t52 * t279 - 0.7e1 / 0.27e2 * t17 * t279;
  t302 = t8 * t278;
  t306 = t7 * t273;
  t312 = -t24 * t237 * t239 / 0.36e2 + t237 * t239 * t29 * t31 / 0.36e2 - 0.6e1 * t155 * t61 * t175 - 0.5e1 / 0.12e2 * t118 * t250 * t31 - 0.6e1 * t256 * t257 - 0.2e1 / 0.3e1 * t30 * t121 - t49 * t284 + t237 * t239 * t33 / 0.36e2 - 0.5e1 / 0.12e2 * t118 * t239 * t42 + 0.6e1 * t256 * t27 * t257 - t118 * t250 / 0.12e2 + 0.6e1 * t155 * t59 * t175 + t49 * t27 * t284 + 0.7e1 / 0.36e2 * t70 * t302 * t29 + t84 * t306 * t35 / 0.4e1 - 0.13e2 / 0.36e2 * t37 * t279;
  t327 = t236 * t3;
  t330 = 0.1e1 / t9 / t238;
  t378 = -t187 * t306 * t29 / 0.36e2 - t187 * t306 * t33 / 0.9e1 + 0.7e1 / 0.27e2 * t70 * t302 * t33 + 0.13e2 / 0.27e2 * t84 * t306 * t42 - 0.7e1 / 0.27e2 * t44 * t279 + t327 * t5 * t8 * t330 * t4 * t42 / 0.144e3 + t196 * t197 * t306 / 0.9e1 - t187 * t306 * t71 / 0.9e1 - 0.7e1 / 0.27e2 * t64 * t6 * t302 + 0.7e1 / 0.27e2 * t70 * t302 * t71 + 0.13e2 / 0.27e2 * t84 * t306 * t89 - 0.7e1 / 0.27e2 * t37 * t18 * t278 * t31 + t327 * t18 * t330 * t4 * t89 / 0.144e3 - t155 * t69 * t18 * t54 * t156 / 0.2e1 - t129 * t18 * t135 * t59 / 0.3e1 + t129 * t18 * t54 * t175 / 0.4e1 + t49 * t186 * t166 * t144 * t59 / 0.12e2;
  t381 = -0.3e1 * t182 - t184 / 0.6e1 + t189 / 0.12e2 - t192 / 0.6e1 + 0.5e1 / 0.12e2 * t194 - t199 / 0.12e2 + t202 / 0.12e2 - t205 / 0.3e1 - t208 / 0.2e1 + t210 / 0.3e1 + rho[0] * (t378 + t312);
  if(v3rho3 != NULL && (p->info->flags & XC_FLAGS_HAVE_KXC))
    v3rho3[0] = t381 + t225;

  if(order < 4) return;



}


static inline void
func_ferr(const xc_func_type *p, int order, const double *rho, double *zk, double *vrho, double *v2rho2, double *v3rho3)
{
  double t1, t2, t3, t4, t5, t6, t7, t8;
  double t9, t10, t12, t13, t17, t18, t19, t22;
  double t24, t25, t26, t27, t28, t29, t30, t31;
  double t32, t33, t34, t35, t37, t41, t42, t44;
  double t46, t47, t49, t52, t54, t55, t59, t60;
  double t61, t62, t63, t64, t65, t67, t69, t70;
  double t71, t73, t76, t79, t81, t83, t84, t85;
  double t88, t89, t91, t95, t97, t100, t102, t104;
  double t117, t118, t119, t120, t121, t123, t127, t129;
  double t132, t135, t136, t138, t141, t144, t145, t147;
  double t151, t155, t156, t158, t160, t165, t166, t170;
  double t175, t177, t180, t182, t184, t186, t187, t189;
  double t192, t194, t196, t197, t199, t202, t205, t208;
  double t210, t212, t225, t236, t237, t238, t239, t250;
  double t254, t256, t257, t273, t278, t279, t284, t302;
  double t306, t312, t327, t330, t378, t381;

  lda_c_pz_params *params;

  assert(p->params != NULL);
  params = (lda_c_pz_params * )(p->params);

  t1 = params->gamma[1];
  t2 = params->beta1[1];
  t3 = M_CBRT3;
  t4 = 0.1e1 / M_PI;
  t5 = POW_1_3(t4);
  t6 = t3 * t5;
  t7 = M_CBRT4;
  t8 = t7 * t7;
  t9 = POW_1_3(rho[0]);
  t10 = 0.1e1 / t9;
  t12 = t6 * t8 * t10;
  t13 = sqrt(t12);
  t17 = params->beta2[1] * t3;
  t18 = t5 * t8;
  t19 = t18 * t10;
  t22 = 0.1e1 + t2 * t13 / 0.2e1 + t17 * t19 / 0.4e1;
  t24 = t1 / t22;
  t25 = t12 / 0.4e1;
  t26 = 0.1e1 - t25;
  t27 = Heaviside(t26);
  t28 = t24 * t27;
  t29 = params->a[1];
  t30 = t27 * t29;
  t31 = log(t25);
  t32 = t30 * t31;
  t33 = params->b[1];
  t34 = t27 * t33;
  t35 = params->c[1];
  t37 = t27 * t35 * t3;
  t41 = t37 * t18 * t10 * t31 / 0.4e1;
  t42 = params->d[1];
  t44 = t27 * t42 * t3;
  t46 = t44 * t19 / 0.4e1;
  if(zk != NULL && (p->info->flags & XC_FLAGS_HAVE_EXC))
    *zk = t24 - t28 + t32 + t34 + t41 + t46;

  if(order < 1) return;


  t47 = t22 * t22;
  t49 = t1 / t47;
  t52 = t2 / t13 * t3;
  t54 = 0.1e1 / t9 / rho[0];
  t55 = t18 * t54;
  t59 = -t17 * t55 / 0.12e2 - t52 * t55 / 0.12e2;
  t60 = t49 * t59;
  t61 = t27 * t59;
  t62 = t49 * t61;
  t63 = 0.0;
  t64 = t24 * t63;
  t65 = t8 * t54;
  t67 = t64 * t6 * t65;
  t69 = t63 * t3;
  t70 = t69 * t5;
  t71 = t29 * t31;
  t73 = t70 * t65 * t71;
  t76 = t30 / rho[0];
  t79 = t70 * t65 * t33;
  t81 = t3 * t3;
  t83 = t5 * t5;
  t84 = t63 * t81 * t83;
  t85 = t9 * t9;
  t88 = t7 / t85 / rho[0];
  t89 = t35 * t31;
  t91 = t84 * t88 * t89;
  t95 = t37 * t18 * t54 * t31;
  t97 = t37 * t55;
  t100 = t84 * t88 * t42;
  t102 = t44 * t55;
  t104 = -t60 + t62 - t67 / 0.12e2 + t73 / 0.12e2 - t76 / 0.3e1 + t79 / 0.12e2 + t91 / 0.12e2 - t95 / 0.12e2 - t97 / 0.12e2 + t100 / 0.12e2 - t102 / 0.12e2;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vrho[0] = rho[0] * t104 + t24 - t28 + t32 + t34 + t41 + t46;

  if(order < 2) return;


  t117 = 0.0;
  t118 = t117 * t4;
  t119 = rho[0] * rho[0];
  t120 = t119 * rho[0];
  t121 = 0.1e1 / t120;
  t123 = t118 * t121 * t42;
  t127 = t118 * t121 * t35 * t31;
  t129 = t49 * t69;
  t132 = t129 * t18 * t54 * t59;
  t135 = 0.1e1 / t9 / t119;
  t136 = t8 * t135;
  t138 = t64 * t6 * t136;
  t141 = t70 * t136 * t71;
  t144 = 0.1e1 / t85 / t119;
  t145 = t7 * t144;
  t147 = t84 * t145 * t89;
  t151 = t37 * t18 * t135 * t31;
  t155 = t1 / t47 / t22;
  t156 = t59 * t59;
  t158 = t155 * t27 * t156;
  t160 = t155 * t156;
  t165 = t2 / t13 / t12 * t81;
  t166 = t83 * t7;
  t170 = t18 * t135;
  t175 = -t165 * t166 * t144 / 0.18e2 + t52 * t170 / 0.9e1 + t17 * t170 / 0.9e1;
  t177 = t49 * t27 * t175;
  t180 = t30 / t119;
  t182 = t49 * t175;
  t184 = t70 * t136 * t29;
  t186 = t117 * t81;
  t187 = t186 * t83;
  t189 = t187 * t145 * t33;
  t192 = t84 * t145 * t35;
  t194 = t37 * t170;
  t196 = t24 * t117;
  t197 = t81 * t83;
  t199 = t196 * t197 * t145;
  t202 = t187 * t145 * t71;
  t205 = t70 * t136 * t33;
  t208 = t84 * t145 * t42;
  t210 = t44 * t170;
  t212 = t180 / 0.3e1 - t182 - t184 / 0.18e2 + t189 / 0.36e2 - t192 / 0.18e2 + 0.5e1 / 0.36e2 * t194 - t199 / 0.36e2 + t202 / 0.36e2 - t205 / 0.9e1 - t208 / 0.6e1 + t210 / 0.9e1;
  if(v2rho2 != NULL && (p->info->flags & XC_FLAGS_HAVE_FXC))
    v2rho2[0] = -0.2e1 * t60 + 0.2e1 * t62 - t67 / 0.6e1 + t73 / 0.6e1 - 0.2e1 / 0.3e1 * t76 + t79 / 0.6e1 + t91 / 0.6e1 - t95 / 0.6e1 - t97 / 0.6e1 + t100 / 0.6e1 - t102 / 0.6e1 + rho[0] * (t212 + t123 / 0.12e2 + t127 / 0.12e2 + t132 / 0.6e1 + t138 / 0.9e1 - t141 / 0.9e1 - t147 / 0.6e1 + t151 / 0.9e1 - 0.2e1 * t158 + 0.2e1 * t160 + t177);

  if(order < 3) return;


  t225 = t123 / 0.4e1 + t127 / 0.4e1 + t132 / 0.2e1 + t138 / 0.3e1 - t141 / 0.3e1 - t147 / 0.2e1 + t151 / 0.3e1 - 0.6e1 * t158 + 0.6e1 * t160 + 0.3e1 * t177 + t180;
  t236 = 0.0;
  t237 = t236 * t4;
  t238 = t119 * t119;
  t239 = 0.1e1 / t238;
  t250 = t239 * t35;
  t254 = t47 * t47;
  t256 = t1 / t254;
  t257 = t156 * t59;
  t273 = 0.1e1 / t85 / t120;
  t278 = 0.1e1 / t9 / t120;
  t279 = t18 * t278;
  t284 = -t2 / t13 / t197 / t7 * t85 * t4 * t239 / 0.12e2 + 0.2e1 / 0.9e1 * t165 * t166 * t273 - 0.7e1 / 0.27e2 * t52 * t279 - 0.7e1 / 0.27e2 * t17 * t279;
  t302 = t8 * t278;
  t306 = t7 * t273;
  t312 = -t24 * t237 * t239 / 0.36e2 + t237 * t239 * t29 * t31 / 0.36e2 - 0.6e1 * t155 * t61 * t175 - 0.5e1 / 0.12e2 * t118 * t250 * t31 - 0.6e1 * t256 * t257 - 0.2e1 / 0.3e1 * t30 * t121 - t49 * t284 + t237 * t239 * t33 / 0.36e2 - 0.5e1 / 0.12e2 * t118 * t239 * t42 + 0.6e1 * t256 * t27 * t257 - t118 * t250 / 0.12e2 + 0.6e1 * t155 * t59 * t175 + t49 * t27 * t284 + 0.7e1 / 0.36e2 * t70 * t302 * t29 + t84 * t306 * t35 / 0.4e1 - 0.13e2 / 0.36e2 * t37 * t279;
  t327 = t236 * t3;
  t330 = 0.1e1 / t9 / t238;
  t378 = -t187 * t306 * t29 / 0.36e2 - t187 * t306 * t33 / 0.9e1 + 0.7e1 / 0.27e2 * t70 * t302 * t33 + 0.13e2 / 0.27e2 * t84 * t306 * t42 - 0.7e1 / 0.27e2 * t44 * t279 + t327 * t5 * t8 * t330 * t4 * t42 / 0.144e3 + t196 * t197 * t306 / 0.9e1 - t187 * t306 * t71 / 0.9e1 - 0.7e1 / 0.27e2 * t64 * t6 * t302 + 0.7e1 / 0.27e2 * t70 * t302 * t71 + 0.13e2 / 0.27e2 * t84 * t306 * t89 - 0.7e1 / 0.27e2 * t37 * t18 * t278 * t31 + t327 * t18 * t330 * t4 * t89 / 0.144e3 + t129 * t18 * t54 * t175 / 0.4e1 + t49 * t186 * t166 * t144 * t59 / 0.12e2 - t155 * t69 * t18 * t54 * t156 / 0.2e1 - t129 * t18 * t135 * t59 / 0.3e1;
  t381 = -0.3e1 * t182 - t184 / 0.6e1 + t189 / 0.12e2 - t192 / 0.6e1 + 0.5e1 / 0.12e2 * t194 - t199 / 0.12e2 + t202 / 0.12e2 - t205 / 0.3e1 - t208 / 0.2e1 + t210 / 0.3e1 + rho[0] * (t378 + t312);
  if(v3rho3 != NULL && (p->info->flags & XC_FLAGS_HAVE_KXC))
    v3rho3[0] = t381 + t225;

  if(order < 4) return;



}


static inline void
func_pol(const xc_func_type *p, int order, const double *rho, double *zk, double *vrho, double *v2rho2, double *v3rho3)
{
  double t1, t2, t3, t4, t5, t6, t7, t8;
  double t9, t10, t11, t13, t14, t18, t19, t20;
  double t23, t25, t26, t27, t28, t29, t30, t31;
  double t32, t33, t34, t35, t36, t38, t40, t42;
  double t43, t45, t47, t48, t49, t53, t56, t58;
  double t60, t61, t63, t65, t67, t70, t72, t75;
  double t76, t77, t78, t79, t80, t82, t83, t85;
  double t87, t90, t91, t92, t94, t95, t97, t99;
  double t100, t104, t105, t106, t107, t108, t109, t110;
  double t111, t112, t113, t114, t115, t116, t118, t119;
  double t120, t121, t123, t124, t125, t127, t128, t129;
  double t132, t133, t135, t136, t138, t139, t140, t141;
  double t142, t144, t145, t146, t147, t148, t150, t152;
  double t156, t158, t160, t163, t172, t185, t186, t187;
  double t189, t190, t191, t192, t193, t195, t198, t200;
  double t201, t203, t205, t208, t210, t211, t213, t214;
  double t215, t216, t217, t219, t220, t221, t222, t223;
  double t224, t225, t226, t227, t228, t229, t231, t232;
  double t235, t236, t239, t240, t242, t243, t244, t245;
  double t247, t249, t250, t252, t253, t257, t258, t263;
  double t265, t266, t267, t268, t270, t271, t272, t273;
  double t274, t276, t277, t279, t280, t281, t282, t283;
  double t284, t285, t286, t287, t289, t290, t291, t293;
  double t294, t296, t297, t298, t299, t300, t307, t308;
  double t314, t321, t332, t353, t360, t361, t362, t364;
  double t365, t367, t368, t370, t371, t372, t383, t386;
  double t387, t388, t393, t395, t397, t399, t400, t401;
  double t402, t403, t406, t408, t411, t412, t413, t416;
  double t419, t421, t422, t426, t427, t430, t433, t436;
  double t439, t441, t442, t443, t446, t447, t451, t454;
  double t457, t460, t462, t463, t468, t469, t472, t474;
  double t475, t478, t481, t484, t485, t487, t490, t493;
  double t495, t496, t497, t498, t499, t500, t502, t506;
  double t509, t510, t513, t516, t517, t519, t520, t523;
  double t525, t528, t534, t536, t539, t546, t548, t551;
  double t552, t554, t556, t557, t580, t587, t599, t600;
  double t602, t603, t610, t612, t613, t617, t631, t644;
  double t645, t649, t654, t656, t659, t662, t666, t668;
  double t671, t674, t676, t678, t692, t699, t704, t708;
  double t713, t723, t727, t729, t732, t735, t742, t744;
  double t748, t761, t764, t765, t766, t767, t770, t771;
  double t772, t773, t774, t775, t776, t777, t778, t779;
  double t780, t781, t782, t783, t784, t785, t786, t787;
  double t788, t789, t792, t795, t825, t829, t830, t831;
  double t834, t845, t862, t866, t875, t889, t894;

  lda_c_pz_params *params;

  assert(p->params != NULL);
  params = (lda_c_pz_params * )(p->params);

  t1 = params->gamma[0];
  t2 = params->beta1[0];
  t3 = M_CBRT3;
  t4 = 0.1e1 / M_PI;
  t5 = POW_1_3(t4);
  t6 = t3 * t5;
  t7 = M_CBRT4;
  t8 = t7 * t7;
  t9 = rho[0] + rho[1];
  t10 = POW_1_3(t9);
  t11 = 0.1e1 / t10;
  t13 = t6 * t8 * t11;
  t14 = sqrt(t13);
  t18 = params->beta2[0] * t3;
  t19 = t5 * t8;
  t20 = t19 * t11;
  t23 = 0.1e1 + t2 * t14 / 0.2e1 + t18 * t20 / 0.4e1;
  t25 = t1 / t23;
  t26 = t13 / 0.4e1;
  t27 = 0.1e1 - t26;
  t28 = Heaviside(t27);
  t29 = t25 * t28;
  t30 = params->a[0];
  t31 = t28 * t30;
  t32 = log(t26);
  t33 = t31 * t32;
  t34 = params->b[0];
  t35 = t28 * t34;
  t36 = params->c[0];
  t38 = t28 * t36 * t3;
  t40 = t19 * t11 * t32;
  t42 = t38 * t40 / 0.4e1;
  t43 = params->d[0];
  t45 = t28 * t43 * t3;
  t47 = t45 * t20 / 0.4e1;
  t48 = params->gamma[1];
  t49 = params->beta1[1];
  t53 = params->beta2[1] * t3;
  t56 = 0.1e1 + t49 * t14 / 0.2e1 + t53 * t20 / 0.4e1;
  t58 = t48 / t56;
  t60 = params->a[1];
  t61 = t28 * t60;
  t63 = params->b[1];
  t65 = params->c[1];
  t67 = t28 * t65 * t3;
  t70 = params->d[1];
  t72 = t28 * t70 * t3;
  t75 = t58 - t58 * t28 + t61 * t32 + t28 * t63 + t67 * t40 / 0.4e1 + t72 * t20 / 0.4e1 - t25 + t29 - t33 - t35 - t42 - t47;
  t76 = rho[0] - rho[1];
  t77 = 0.1e1 / t9;
  t78 = t76 * t77;
  t79 = 0.1e1 + t78;
  t80 = POW_1_3(t79);
  t82 = 0.1e1 - t78;
  t83 = POW_1_3(t82);
  t85 = t80 * t79 + t83 * t82 - 0.2e1;
  t87 = M_CBRT2;
  t90 = 0.1e1 / (0.2e1 * t87 - 0.2e1);
  t91 = t75 * t85 * t90;
  if(zk != NULL && (p->info->flags & XC_FLAGS_HAVE_EXC))
    *zk = t25 - t29 + t33 + t35 + t42 + t47 + t91;

  if(order < 1) return;


  t92 = t23 * t23;
  t94 = t1 / t92;
  t95 = 0.1e1 / t14;
  t97 = t2 * t95 * t3;
  t99 = 0.1e1 / t10 / t9;
  t100 = t19 * t99;
  t104 = -t18 * t100 / 0.12e2 - t97 * t100 / 0.12e2;
  t105 = t94 * t104;
  t106 = t28 * t104;
  t107 = t94 * t106;
  t108 = 0.0;
  t109 = t25 * t108;
  t110 = t8 * t99;
  t111 = t6 * t110;
  t112 = t109 * t111;
  t113 = t112 / 0.12e2;
  t114 = t108 * t3;
  t115 = t114 * t5;
  t116 = t30 * t32;
  t118 = t115 * t110 * t116;
  t119 = t118 / 0.12e2;
  t120 = t31 * t77;
  t121 = t120 / 0.3e1;
  t123 = t115 * t110 * t34;
  t124 = t123 / 0.12e2;
  t125 = t3 * t3;
  t127 = t5 * t5;
  t128 = t108 * t125 * t127;
  t129 = t10 * t10;
  t132 = t7 / t129 / t9;
  t133 = t36 * t32;
  t135 = t128 * t132 * t133;
  t136 = t135 / 0.12e2;
  t138 = t19 * t99 * t32;
  t139 = t38 * t138;
  t140 = t139 / 0.12e2;
  t141 = t38 * t100;
  t142 = t141 / 0.12e2;
  t144 = t128 * t132 * t43;
  t145 = t144 / 0.12e2;
  t146 = t45 * t100;
  t147 = t146 / 0.12e2;
  t148 = t56 * t56;
  t150 = t48 / t148;
  t152 = t49 * t95 * t3;
  t156 = -t152 * t100 / 0.12e2 - t53 * t100 / 0.12e2;
  t158 = t28 * t156;
  t160 = t58 * t108;
  t163 = t60 * t32;
  t172 = t65 * t32;
  t185 = -t150 * t156 + t150 * t158 - t160 * t111 / 0.12e2 + t115 * t110 * t163 / 0.12e2 - t61 * t77 / 0.3e1 + t115 * t110 * t63 / 0.12e2 + t128 * t132 * t172 / 0.12e2 - t67 * t138 / 0.12e2 - t67 * t100 / 0.12e2 + t128 * t132 * t70 / 0.12e2 - t72 * t100 / 0.12e2;
  t186 = t105 - t107 + t113 - t119 + t121 - t124 - t136 + t140 + t142 - t145 + t147;
  t187 = t186 + t185;
  t189 = t187 * t85 * t90;
  t190 = t9 * t9;
  t191 = 0.1e1 / t190;
  t192 = t76 * t191;
  t193 = t77 - t192;
  t195 = -t193;
  t198 = 0.4e1 / 0.3e1 * t80 * t193 + 0.4e1 / 0.3e1 * t83 * t195;
  t200 = t75 * t198 * t90;
  t201 = -t105 + t107 - t113 + t119 - t121 + t124 + t136 - t140 - t142 + t145 - t147 + t189 + t200;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vrho[0] = t9 * t201 + t25 - t29 + t33 + t35 + t42 + t47 + t91;

  t203 = -t77 - t192;
  t205 = -t203;
  t208 = 0.4e1 / 0.3e1 * t80 * t203 + 0.4e1 / 0.3e1 * t83 * t205;
  t210 = t75 * t208 * t90;
  t211 = -t105 + t107 - t113 + t119 - t121 + t124 + t136 - t140 - t142 + t145 - t147 + t189 + t210;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vrho[1] = t9 * t211 + t25 - t29 + t33 + t35 + t42 + t47 + t91;

  if(order < 2) return;


  t213 = t139 / 0.6e1;
  t214 = 0.2e1 * t107;
  t215 = 0.2e1 * t105;
  t216 = 0.2e1 / 0.3e1 * t120;
  t217 = 0.2e1 * t189;
  t219 = t123 / 0.6e1;
  t220 = t141 / 0.6e1;
  t221 = t144 / 0.6e1;
  t222 = t112 / 0.6e1;
  t223 = t118 / 0.6e1;
  t224 = t135 / 0.6e1;
  t225 = t146 / 0.6e1;
  t226 = 0.0;
  t227 = t226 * t4;
  t228 = t190 * t9;
  t229 = 0.1e1 / t228;
  t231 = t227 * t229 * t43;
  t232 = t231 / 0.12e2;
  t235 = t227 * t229 * t36 * t32;
  t236 = t235 / 0.12e2;
  t239 = t1 / t92 / t23;
  t240 = t104 * t104;
  t242 = t239 * t28 * t240;
  t243 = 0.2e1 * t242;
  t244 = t239 * t240;
  t245 = 0.2e1 * t244;
  t247 = 0.1e1 / t14 / t13;
  t249 = t2 * t247 * t125;
  t250 = t127 * t7;
  t252 = 0.1e1 / t129 / t190;
  t253 = t250 * t252;
  t257 = 0.1e1 / t10 / t190;
  t258 = t19 * t257;
  t263 = -t249 * t253 / 0.18e2 + t97 * t258 / 0.9e1 + t18 * t258 / 0.9e1;
  t265 = t94 * t28 * t263;
  t266 = t31 * t191;
  t267 = t266 / 0.3e1;
  t268 = t8 * t257;
  t270 = t115 * t268 * t30;
  t271 = t270 / 0.18e2;
  t272 = t226 * t125;
  t273 = t272 * t127;
  t274 = t7 * t252;
  t276 = t273 * t274 * t34;
  t277 = t276 / 0.36e2;
  t279 = t128 * t274 * t36;
  t280 = t279 / 0.18e2;
  t281 = t38 * t258;
  t282 = 0.5e1 / 0.36e2 * t281;
  t283 = t25 * t226;
  t284 = t125 * t127;
  t285 = t284 * t274;
  t286 = t283 * t285;
  t287 = t286 / 0.36e2;
  t289 = t273 * t274 * t116;
  t290 = t289 / 0.36e2;
  t291 = t232 + t236 - t243 + t245 + t265 + t267 - t271 + t277 - t280 + t282 - t287 + t290;
  t293 = t115 * t268 * t34;
  t294 = t293 / 0.9e1;
  t296 = t128 * t274 * t43;
  t297 = t296 / 0.6e1;
  t298 = t45 * t258;
  t299 = t298 / 0.9e1;
  t300 = t94 * t263;
  t307 = t48 / t148 / t56;
  t308 = t156 * t156;
  t314 = t49 * t247 * t125;
  t321 = -t314 * t253 / 0.18e2 + t152 * t258 / 0.9e1 + t53 * t258 / 0.9e1;
  t332 = -t265 + t227 * t229 * t70 / 0.12e2 - 0.2e1 * t307 * t28 * t308 + t150 * t28 * t321 + t271 - t277 + t280 - t282 + t294 + t297 - t299;
  t353 = t58 * t226;
  t360 = t6 * t268;
  t361 = t109 * t360;
  t362 = t361 / 0.9e1;
  t364 = t115 * t268 * t116;
  t365 = t364 / 0.9e1;
  t367 = t128 * t274 * t133;
  t368 = t367 / 0.6e1;
  t370 = t19 * t257 * t32;
  t371 = t38 * t370;
  t372 = t371 / 0.9e1;
  t383 = t94 * t114;
  t386 = t383 * t19 * t99 * t104;
  t387 = t386 / 0.6e1;
  t388 = t150 * t114;
  t393 = t273 * t274 * t163 / 0.36e2 - t362 + t365 + t368 - t372 + t160 * t360 / 0.9e1 - t115 * t268 * t163 / 0.9e1 - t128 * t274 * t172 / 0.6e1 + t67 * t370 / 0.9e1 - t387 + t388 * t19 * t99 * t156 / 0.6e1;
  t395 = t393 - t115 * t268 * t60 / 0.18e2 + t273 * t274 * t63 / 0.36e2 - t128 * t274 * t65 / 0.18e2 + 0.5e1 / 0.36e2 * t67 * t258 - t115 * t268 * t63 / 0.9e1 - t128 * t274 * t70 / 0.6e1 + t72 * t258 / 0.9e1 + t287 - t290 - t353 * t285 / 0.36e2 + t332 + t227 * t229 * t65 * t32 / 0.12e2 - t236 - t245 - t267 + t300 + 0.2e1 * t307 * t308 + t61 * t191 / 0.3e1 - t150 * t321 - t232 + t243;
  t397 = t395 * t85 * t90;
  t399 = t187 * t198 * t90;
  t400 = 0.2e1 * t399;
  t401 = t80 * t80;
  t402 = 0.1e1 / t401;
  t403 = t193 * t193;
  t406 = t76 * t229;
  t408 = -0.2e1 * t191 + 0.2e1 * t406;
  t411 = t83 * t83;
  t412 = 0.1e1 / t411;
  t413 = t195 * t195;
  t416 = -t408;
  t419 = 0.4e1 / 0.9e1 * t402 * t403 + 0.4e1 / 0.3e1 * t80 * t408 + 0.4e1 / 0.9e1 * t412 * t413 + 0.4e1 / 0.3e1 * t83 * t416;
  t421 = t75 * t419 * t90;
  t422 = -t294 - t297 + t299 - t300 + t397 + t400 + t421 + t387 + t362 - t365 - t368 + t372;
  if(v2rho2 != NULL && (p->info->flags & XC_FLAGS_HAVE_FXC))
    v2rho2[0] = -t213 + t214 - t215 - t216 + t217 + 0.2e1 * t200 + t219 - t220 + t221 - t222 + t223 + t224 - t225 + t9 * (t422 + t291);

  t426 = t187 * t208 * t90;
  t427 = t402 * t203;
  t430 = t80 * t76;
  t433 = t412 * t205;
  t436 = t83 * t76;
  t439 = 0.4e1 / 0.9e1 * t427 * t193 + 0.8e1 / 0.3e1 * t430 * t229 + 0.4e1 / 0.9e1 * t433 * t195 - 0.8e1 / 0.3e1 * t436 * t229;
  t441 = t75 * t439 * t90;
  t442 = t236 + t245 + t267 - t300 + t397 + t399 + t426 + t441 + t232 - t243 + t265 - t271;
  t443 = t277 - t280 + t282 - t294 - t297 + t299 - t287 + t290 + t362 - t365 - t368 + t372 + t387;
  if(v2rho2 != NULL && (p->info->flags & XC_FLAGS_HAVE_FXC))
    v2rho2[1] = -t213 + t214 - t215 - t216 + t217 + t200 + t219 - t220 + t221 - t222 + t223 + t224 - t225 + t9 * (t443 + t442) + t210;

  t446 = 0.2e1 * t426;
  t447 = t203 * t203;
  t451 = 0.2e1 * t191 + 0.2e1 * t406;
  t454 = t205 * t205;
  t457 = -t451;
  t460 = 0.4e1 / 0.9e1 * t402 * t447 + 0.4e1 / 0.3e1 * t80 * t451 + 0.4e1 / 0.9e1 * t412 * t454 + 0.4e1 / 0.3e1 * t83 * t457;
  t462 = t75 * t460 * t90;
  t463 = -t294 - t297 + t299 - t300 + t397 + t446 + t462 + t387 + t362 - t365 - t368 + t372;
  if(v2rho2 != NULL && (p->info->flags & XC_FLAGS_HAVE_FXC))
    v2rho2[2] = -t213 + t214 - t215 - t216 + t217 + t219 - t220 + t221 - t222 + t223 + t224 - t225 + t9 * (t463 + t291) + 0.2e1 * t210;

  if(order < 3) return;


  t468 = 0.1e1 / t10 / t228;
  t469 = t8 * t468;
  t472 = 0.7e1 / 0.36e2 * t115 * t469 * t30;
  t474 = 0.1e1 / t129 / t228;
  t475 = t7 * t474;
  t478 = t273 * t475 * t30 / 0.36e2;
  t481 = t273 * t475 * t34 / 0.9e1;
  t484 = t128 * t475 * t36 / 0.4e1;
  t485 = t19 * t468;
  t487 = 0.13e2 / 0.36e2 * t38 * t485;
  t490 = 0.7e1 / 0.27e2 * t115 * t469 * t34;
  t493 = 0.13e2 / 0.27e2 * t128 * t475 * t43;
  t495 = 0.7e1 / 0.27e2 * t45 * t485;
  t496 = 0.0;
  t497 = t496 * t4;
  t498 = t190 * t190;
  t499 = 0.1e1 / t498;
  t500 = t497 * t499;
  t502 = t25 * t500 / 0.36e2;
  t506 = t497 * t499 * t30 * t32 / 0.36e2;
  t509 = 0.6e1 * t239 * t106 * t263;
  t510 = t499 * t36;
  t513 = 0.5e1 / 0.12e2 * t227 * t510 * t32;
  t516 = 0.5e1 / 0.12e2 * t227 * t499 * t43;
  t517 = t92 * t92;
  t519 = t1 / t517;
  t520 = t240 * t104;
  t523 = 0.6e1 * t519 * t28 * t520;
  t525 = t227 * t510 / 0.12e2;
  t528 = 0.6e1 * t239 * t104 * t263;
  t534 = 0.1e1 / t14 / t284 / t7 * t129 / 0.4e1;
  t536 = t4 * t499;
  t539 = t250 * t474;
  t546 = -t2 * t534 * t536 / 0.3e1 + 0.2e1 / 0.9e1 * t249 * t539 - 0.7e1 / 0.27e2 * t97 * t485 - 0.7e1 / 0.27e2 * t18 * t485;
  t548 = t94 * t28 * t546;
  t551 = t497 * t499 * t34 / 0.36e2;
  t552 = t472 - t478 - t481 + t484 - t487 + t490 + t493 - t495 - t502 + t506 - t509 - t513 - t516 + t523 - t525 + t528 + t548 + t551;
  t554 = 0.6e1 * t519 * t520;
  t556 = 0.2e1 / 0.3e1 * t31 * t229;
  t557 = t94 * t546;
  t580 = 0.7e1 / 0.36e2 * t115 * t469 * t60 - t273 * t475 * t60 / 0.36e2 - t273 * t475 * t63 / 0.9e1 + t128 * t475 * t65 / 0.4e1 - 0.13e2 / 0.36e2 * t67 * t485 + 0.7e1 / 0.27e2 * t115 * t469 * t63 + 0.13e2 / 0.27e2 * t128 * t475 * t70 - 0.7e1 / 0.27e2 * t72 * t485 - t472 + t478 + t481 - t484 + t487 - t490 - t493 + t495;
  t587 = t499 * t65;
  t599 = t496 * t3;
  t600 = t599 * t19;
  t602 = 0.1e1 / t10 / t498;
  t603 = t602 * t4;
  t610 = t148 * t148;
  t612 = t48 / t610;
  t613 = t308 * t156;
  t617 = t502 - t506 - t58 * t500 / 0.36e2 + t497 * t499 * t60 * t32 / 0.36e2 - 0.5e1 / 0.12e2 * t227 * t587 * t32 - 0.6e1 * t307 * t158 * t321 + t509 + t513 + t150 * t272 * t250 * t252 * t156 / 0.12e2 + t600 * t603 * t172 / 0.144e3 + t516 - t523 + t525 - t528 - t548 - 0.5e1 / 0.12e2 * t227 * t499 * t70 + 0.6e1 * t612 * t28 * t613;
  t631 = -t49 * t534 * t536 / 0.3e1 + 0.2e1 / 0.9e1 * t314 * t539 - 0.7e1 / 0.27e2 * t152 * t485 - 0.7e1 / 0.27e2 * t53 * t485;
  t644 = t599 * t5;
  t645 = t8 * t602;
  t649 = t644 * t645 * t4 * t43 / 0.144e3;
  t654 = t284 * t475;
  t656 = t283 * t654 / 0.9e1;
  t659 = t273 * t475 * t116 / 0.9e1;
  t662 = 0.6e1 * t307 * t156 * t321 + t150 * t28 * t631 - t551 + t497 * t499 * t63 / 0.36e2 - t227 * t587 / 0.12e2 + t554 + t556 + t557 - 0.6e1 * t612 * t613 - 0.2e1 / 0.3e1 * t61 * t229 - t150 * t631 - t649 + t644 * t645 * t4 * t70 / 0.144e3 - t656 + t659 + t353 * t654 / 0.9e1;
  t666 = t6 * t469;
  t668 = 0.7e1 / 0.27e2 * t109 * t666;
  t671 = 0.7e1 / 0.27e2 * t115 * t469 * t116;
  t674 = 0.13e2 / 0.27e2 * t128 * t475 * t133;
  t676 = t19 * t468 * t32;
  t678 = 0.7e1 / 0.27e2 * t38 * t676;
  t692 = t383 * t19 * t257 * t104 / 0.3e1;
  t699 = t600 * t603 * t133 / 0.144e3;
  t704 = t239 * t114 * t19 * t99 * t240 / 0.2e1;
  t708 = t383 * t19 * t99 * t263 / 0.4e1;
  t713 = t94 * t272 * t250 * t252 * t104 / 0.12e2;
  t723 = -t273 * t475 * t163 / 0.9e1 + t668 - t671 - t674 + t678 - 0.7e1 / 0.27e2 * t160 * t666 + 0.7e1 / 0.27e2 * t115 * t469 * t163 + 0.13e2 / 0.27e2 * t128 * t475 * t172 - 0.7e1 / 0.27e2 * t67 * t676 + t692 - t388 * t19 * t257 * t156 / 0.3e1 - t699 + t704 - t708 - t713 - t307 * t114 * t19 * t99 * t308 / 0.2e1 + t388 * t19 * t99 * t321 / 0.4e1;
  t727 = (t723 + t662 + t617 + t580) * t85 * t90;
  t729 = t395 * t198 * t90;
  t732 = t187 * t419 * t90;
  t735 = 0.1e1 / t401 / t79;
  t742 = t76 * t499;
  t744 = 0.6e1 * t229 - 0.6e1 * t742;
  t748 = 0.1e1 / t411 / t82;
  t761 = -t554 - t556 - t557 + t727 + 0.3e1 * t729 + 0.3e1 * t732 + t75 * (-0.8e1 / 0.27e2 * t735 * t403 * t193 + 0.4e1 / 0.3e1 * t402 * t193 * t408 + 0.4e1 / 0.3e1 * t80 * t744 - 0.8e1 / 0.27e2 * t748 * t413 * t195 + 0.4e1 / 0.3e1 * t412 * t195 * t416 - 0.4e1 / 0.3e1 * t83 * t744) * t90 + t649 + t656 - t659 - t668 + t671 + t674 - t678 - t692 + t699 - t704 + t708 + t713;
  t764 = t235 / 0.4e1;
  t765 = 0.6e1 * t244;
  t766 = 0.3e1 * t300;
  t767 = 0.3e1 * t397;
  t770 = t231 / 0.4e1;
  t771 = 0.6e1 * t242;
  t772 = 0.3e1 * t265;
  t773 = t270 / 0.6e1;
  t774 = t9 * (t761 + t552) + t764 + t765 + t266 - t766 + t767 + 0.6e1 * t399 + 0.3e1 * t421 + t770 - t771 + t772 - t773;
  t775 = t276 / 0.12e2;
  t776 = t279 / 0.6e1;
  t777 = 0.5e1 / 0.12e2 * t281;
  t778 = t293 / 0.3e1;
  t779 = t296 / 0.2e1;
  t780 = t298 / 0.3e1;
  t781 = t286 / 0.12e2;
  t782 = t289 / 0.12e2;
  t783 = t361 / 0.3e1;
  t784 = t364 / 0.3e1;
  t785 = t367 / 0.2e1;
  t786 = t371 / 0.3e1;
  t787 = t386 / 0.2e1;
  t788 = t775 - t776 + t777 - t778 - t779 + t780 - t781 + t782 + t783 - t784 - t785 + t786 + t787;
  if(v3rho3 != NULL && (p->info->flags & XC_FLAGS_HAVE_KXC))
    v3rho3[0] = t788 + t774;

  t789 = t472 - t478 - t481 + t484 - t487 + t490 + t493 - t495 - t502 + t506 - t509 - t513 - t516 + t523 - t525 + t528 + t548 + t551 - t554;
  t792 = t395 * t208 * t90;
  t795 = 0.2e1 * t187 * t439 * t90;
  t825 = -t556 - t557 + t727 + 0.2e1 * t729 + t732 + t792 + t795 + t75 * (-0.8e1 / 0.27e2 * t735 * t203 * t403 + 0.16e2 / 0.9e1 * t402 * t76 * t229 * t193 + 0.4e1 / 0.9e1 * t427 * t408 + 0.8e1 / 0.3e1 * t80 * t229 - 0.8e1 * t430 * t499 - 0.8e1 / 0.27e2 * t748 * t205 * t413 - 0.16e2 / 0.9e1 * t412 * t76 * t229 * t195 + 0.4e1 / 0.9e1 * t433 * t416 - 0.8e1 / 0.3e1 * t83 * t229 + 0.8e1 * t436 * t499) * t90 + t649 + t656 - t659 - t668 + t671 + t674 - t678 - t692 + t699 - t704 + t708 + t713;
  t829 = 0.2e1 * t441;
  t830 = t9 * (t825 + t789) + t764 + t765 + t266 - t766 + t767 + 0.4e1 * t399 + t421 + t446 + t829 + t770 - t771 + t772;
  t831 = -t773 + t775 - t776 + t777 - t778 - t779 + t780 - t781 + t782 + t783 - t784 - t785 + t786 + t787;
  if(v3rho3 != NULL && (p->info->flags & XC_FLAGS_HAVE_KXC))
    v3rho3[1] = t831 + t830;

  t834 = t187 * t460 * t90;
  t845 = -0.2e1 * t229 - 0.6e1 * t742;
  t862 = -t556 - t557 + t727 + t729 + 0.2e1 * t792 + t795 + t834 + t75 * (-0.8e1 / 0.27e2 * t735 * t447 * t193 + 0.16e2 / 0.9e1 * t427 * t406 + 0.4e1 / 0.9e1 * t402 * t451 * t193 + 0.4e1 / 0.3e1 * t80 * t845 - 0.8e1 / 0.27e2 * t748 * t454 * t195 - 0.16e2 / 0.9e1 * t433 * t406 + 0.4e1 / 0.9e1 * t412 * t457 * t195 - 0.4e1 / 0.3e1 * t83 * t845) * t90 + t649 + t656 - t659 - t668 + t671 + t674 - t678 - t692 + t699 - t704 + t708 + t713;
  t866 = t9 * (t862 + t789) + t764 + t765 + t266 - t766 + t767 + t400 + 0.4e1 * t426 + t829 + t770 - t771 + t772 + t462;
  if(v3rho3 != NULL && (p->info->flags & XC_FLAGS_HAVE_KXC))
    v3rho3[2] = t831 + t866;

  t875 = -0.6e1 * t229 - 0.6e1 * t742;
  t889 = -t554 - t556 - t557 + t727 + 0.3e1 * t792 + 0.3e1 * t834 + t75 * (-0.8e1 / 0.27e2 * t735 * t447 * t203 + 0.4e1 / 0.3e1 * t427 * t451 + 0.4e1 / 0.3e1 * t80 * t875 - 0.8e1 / 0.27e2 * t748 * t454 * t205 + 0.4e1 / 0.3e1 * t433 * t457 - 0.4e1 / 0.3e1 * t83 * t875) * t90 + t649 + t656 - t659 - t668 + t671 + t674 - t678 - t692 + t699 - t704 + t708 + t713;
  t894 = t9 * (t889 + t552) + t764 + t765 + t266 - t766 + t767 + 0.6e1 * t426 + t770 - t771 + t772 + 0.3e1 * t462 - t773;
  if(v3rho3 != NULL && (p->info->flags & XC_FLAGS_HAVE_KXC))
    v3rho3[3] = t788 + t894;

  if(order < 4) return;



}

