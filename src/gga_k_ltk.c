/*
 Copyright (C) 2006-2007 M.A.L. Marques

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "util.h"

#define XC_GGA_K_LTK          602  /* Thomas-Fermi plus von Weiszaecker correction */
#define XC_GGA_K_LTK1         603 /* von Weiszaecker functional */

typedef struct{
  double a;
} gga_k_ltk_params;

static void 
gga_k_ltk_set_params(xc_func_type *p, double a)
{
	assert(p != NULL && p->params != NULL);
    gga_k_ltk_params *params = (gga_k_ltk_params*)p->params;
	if(a > 0.0) {
		params->a = a;
		return;
	}
	switch(p->info->number) {
		case XC_GGA_K_LTK:
			params->a = 1.3;
			break;
		case XC_GGA_K_LTK1:
			params->a = 1.72;
			break;
	}
}

static void 
gga_k_ltk_init(xc_func_type *p)
{

  assert(p->params == NULL);
  p->params = malloc(sizeof(gga_k_ltk_params));

  /* This automatically sets gamma and lambda depending on the functional chosen.
     We put by default N = 1.0 */
  gga_k_ltk_set_params(p, -1.0);
}

#include "maple2c/gga_exc/gga_k_ltk.c"
#include "work_gga_new.c"

static const func_params_type ltk_ext_params[] = {
  {"a", 1.3, "a"},
};

static void 
ltk_set_ext_params(xc_func_type *p, const double *ext_params)
{
  double a;

  a = get_ext_param(p->info->ext_params, ext_params, 0);
  gga_k_ltk_set_params(p, a);
}

const xc_func_info_type xc_func_info_gga_k_ltk = {
  XC_GGA_K_LTK,
  XC_KINETIC,
  "LTK Pauli term",
  XC_FAMILY_GGA,
  {&xc_ref_Weizsacker1935_431, NULL, NULL, NULL, NULL},
  XC_FLAGS_3D | XC_FLAGS_HAVE_EXC | XC_FLAGS_HAVE_VXC | XC_FLAGS_HAVE_FXC | XC_FLAGS_HAVE_KXC,
  1e-25,
  1, ltk_ext_params, ltk_set_ext_params,
  gga_k_ltk_init, NULL, 
  NULL, work_gga, NULL
};

const xc_func_info_type xc_func_info_gga_k_ltk1 = {
  XC_GGA_K_LTK1,
  XC_KINETIC,
  "Modified LTK Pauli term",
  XC_FAMILY_GGA,
  {&xc_ref_Weizsacker1935_431, NULL, NULL, NULL, NULL},
  XC_FLAGS_3D | XC_FLAGS_HAVE_EXC | XC_FLAGS_HAVE_VXC | XC_FLAGS_HAVE_FXC | XC_FLAGS_HAVE_KXC,
  1e-25,
  1, ltk_ext_params, ltk_set_ext_params,
  gga_k_ltk_init, NULL, 
  NULL, work_gga, NULL
};
