(* type: gga_exc *)
(* prefix:
  gga_k_ltk_params *params;

  assert(p->params != NULL);
  params = (gga_k_ltk_params * )(p->params);
*)

(* ltk_f := x -> params_a_gamma + (params_a_lambda/8)*x^2/K_FACTOR_C: *)
S_FACTOR_C    := 2*(3*Pi^2)^(1/3):
ltk_f := x -> sech(params_a_a*S_FACTOR_C*x):
(*ltk_f := x -> 1.0/cosh(params_a_a*S_FACTOR_C*x):*)

f := (rs, zeta, xt, xs0, xs1) -> gga_kinetic(ltk_f, rs, zeta, xs0, xs1):